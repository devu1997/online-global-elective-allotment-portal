# Generated by Django 2.0 on 2018-08-30 08:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gep_app', '0017_student_past_courses'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='past_courses',
            field=models.ManyToManyField(blank=True, to='gep_app.Course'),
        ),
    ]
